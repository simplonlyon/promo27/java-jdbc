package co.simplon.promo27.entity;

public class Person {
    private Integer id;
    private String name;
    private String firstName;
    private Integer age;

    public Person(String name, String firstName, Integer age) {
        this.name = name;
        this.firstName = firstName;
        this.age = age;
    }
    public Person(Integer id, String name, String firstName, Integer age) {
        this.id = id;
        this.name = name;
        this.firstName = firstName;
        this.age = age;
    }
    public Person() {
    }

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public Integer getAge() {
        return age;
    }
    public void setAge(Integer age) {
        this.age = age;
    } 
}
