package co.simplon.promo27.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


import co.simplon.promo27.entity.Person;

/**
 * Un repository est une classe qui servira à faire les requêtes vers la base de
 * données
 * C'est la seule couche applicativee dans laquelle on aura une dépendance à la
 * bdd et où
 * on verra des requêtes SQL.
 * L'idée de cette classe est de convertir des requêtes SQL en instances d'une
 * entité (ici Person)
 * ou à l'inverse de convertir des instances en requêtes SQL (pour les requêtes
 * de modification)
 */
public class PersonRepository {
    /**
     * Méthode permettant de requêter la table person et de transformer le résultat
     * sous forme de liste de Person
     * @return Une liste d'instance de Person (ou vide si pas de résultat)
     */
    public List<Person> findAll() {
        List<Person> list = new ArrayList<>();
        /*
          On commence par se connecter à la base de donnée via JDBC. Cette opération et
          toutes celles interagissant avec SQL peuvent déclencher des erreurs SQL, on entoure
          donc le tout d'un try-catch pour gérer ces erreurs
         */
        try (Connection connection = Database.connect()) {
            //On prépare la requête SQL qui sera exécuter, ici celle permettant de récupérer toutes les personnes
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM person");
            //On exécute la requête préparée, celle ci renvoie les résultats sous forme de ResultSet
            ResultSet result = stmt.executeQuery();

            /*
             * On parcours chaque ligne de résultat avec cette boucle et pour chaque ligne
             * on crée une nouvelle instance de Person qu'on ajoute à la liste.
             * Pour chaque instance, on va chercher la valeurs des colonnes avec les get...
             */
            while (result.next()) {
                Person person = new Person(
                        result.getInt("id"),
                        result.getString("name"),
                        result.getString("first_name"),
                        result.getInt("age"));
                list.add(person);

            }

        } catch (SQLException e) {
            System.out.println("Error From Repository");
            e.printStackTrace();
        }

        return list;
    }

    public Person findById(int id) {

        try (Connection connection = Database.connect()) {
            /*
            Ici, on fait une requête avec un paramètre, pour se protéger des injections SQL,
            il est essentiel de ne jamais faire de concaténation dans la requête, à la place
            on utilise un système de placeholder, ici le '?' et on utilisera ensuite les
            set... pour assigner des valeurs à ces placeholders
            */
            PreparedStatement stmt = 
            connection.prepareStatement("SELECT * FROM person WHERE id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();

            if (result.next()) {
                return new Person(
                        result.getInt("id"),
                        result.getString("name"),
                        result.getString("first_name"),
                        result.getInt("age"));
            }

        } catch (SQLException e) {
            System.out.println("Error From Repository");
            e.printStackTrace();
        }

        return null;
    }

    public boolean delete(int id) {

        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM person WHERE id=?");
            stmt.setInt(1, id);
            //Si on a bien une ligne qui a été affectée par la requête, on renvoie true
            if(stmt.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException e) {
            System.out.println("Error from repository");
            e.printStackTrace();
        }

        return false;
    }


    public boolean persist(Person person) {
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("INSERT INTO person (name,first_name,age) VALUES (?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, person.getName());
            stmt.setString(2, person.getFirstName());
            stmt.setInt(3, person.getAge());
            
            if(stmt.executeUpdate() == 1) {
                //Récupérer l'id auto incrémenté pour l'assigner à la Person
                ResultSet keys = stmt.getGeneratedKeys();
                keys.next();
                person.setId(keys.getInt(1));
                return true;
            }

            
        } catch (SQLException e) {
            System.out.println("Error from repository");
            e.printStackTrace();
        }
        return false;
    }


     /**
     * Méthode permettant de mettre à jour une personne existant en base de données
     * Elle va mettre à jour tous les champs de la personne donnée
     * @param person La personne à mettre à jour, il faut que ça soit une personne complete avec id
     * @return TRUE si la personne a bien été modifié, FALSE sinon
     */


     public boolean updatePerson(Person person){

    try(Connection connection = Database.connect()) {
    PreparedStatement stmt = connection.prepareStatement("UPDATE person SET name=?,first_name=?,age= ? WHERE id=?");
        stmt.setString(1, person.getName());
        stmt.setString(2, person.getFirstName());
        stmt.setInt(3, person.getAge());
        stmt.setInt(4, person.getId());

        return stmt.executeUpdate() == 1;


    } catch (SQLException e) {
        System.out.println("Error from repository");
        e.printStackTrace();
    }

        return false;

     }
  
}
