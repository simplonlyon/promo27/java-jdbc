package co.simplon.promo27.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.List;

import org.apache.ibatis.jdbc.ScriptRunner;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import co.simplon.promo27.entity.Person;


public class PersonRepositoryTest {

    /**
     * Grâce au @BeforeEach, la méthode setUp se relancera avant chaque test
     * on peut s'en servir pour remettre le système dans un état connu, ici on 
     * relance le script database.sql pour remettre la base de donnée à zéro (avec
     * les 4 insert into connus) entre chaque test
     */
    @BeforeEach
    void setUp() {
        try {
            ScriptRunner runner = new ScriptRunner(Database.connect());
            runner.runScript(new InputStreamReader(getClass().getResourceAsStream("/database.sql")));
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Test
    void findAllShouldReturnAListOfPersons() {
        PersonRepository repo = new PersonRepository();
        List<Person> result = repo.findAll();

        
        //Assertions précises mais un peu plus fragile, dépendante de ce qui se trouve dans la bdd pour de vrai
        assertEquals(4, result.size());
        assertEquals("Name 1", result.get(0).getName());
        assertEquals("FirstName 1", result.get(0).getFirstName());
        assertEquals(35, result.get(0).getAge());
        assertEquals(1, result.get(0).getId());

        //Assertions moins précises mais qui marchent quelque soit ce qu'on a dans la bdd
        assertNotNull(result.get(0).getName());
        assertNotNull(result.get(0).getFirstName());
        assertNotNull(result.get(0).getAge());
        assertNotNull(result.get(0).getId());
    }

    @Test
    void findByIdWithResult() {
        PersonRepository repo = new PersonRepository();
        Person result = repo.findById(1);

        assertEquals("Name 1", result.getName());
        assertEquals("FirstName 1", result.getFirstName());
        assertEquals(35, result.getAge());
        assertEquals(1, result.getId());
    }

    @Test
    void findByIdNoResult() {
        PersonRepository repo = new PersonRepository();
        Person result = repo.findById(1000);

        assertNull(result);
    }

    @Test
    void deleteSuccess() {
        PersonRepository repo = new PersonRepository();

        boolean result = repo.delete(1);
        assertTrue(result);

        //Éventuellement pour être bien sûr que ça a supprimé on peut appeler
        //un findAll en dessous mais alors on part du principe que le findAll marche
        //ce qui n'est pas forcément le cas
        assertEquals(3, repo.findAll().size());
    }

    @Test
    void persistSuccess() {
        PersonRepository repo = new PersonRepository();
        Person person = new Person("test name", "test first name",43);
        assertTrue(repo.persist(person));
        assertEquals(5, person.getId());
    }

    @Test
    void updateSuccess() {
        PersonRepository repo = new PersonRepository();
        Person person = new Person(1, "test name", "test first name",43);
        assertTrue(repo.updatePerson(person));
        
        Person updated = repo.findById(1);
        assertEquals("test name", updated.getName());
        assertEquals("test first name", updated.getFirstName());
    }
}
