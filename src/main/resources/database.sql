-- Active: 1689601538819@@127.0.0.1@8889@p27_jdbc

DROP TABLE IF EXISTS person;

CREATE TABLE person(
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(128) NOT NULL,
    first_name VARCHAR(128) NOT NULL,
    age INT
);

INSERT INTO person (name,first_name,age) VALUES 
('Name 1', 'FirstName 1', 35),
('Name 2', 'FirstName 2', 25),
('Name 3', 'FirstName 3', 18),
('Name 4', 'FirstName 4', 63);