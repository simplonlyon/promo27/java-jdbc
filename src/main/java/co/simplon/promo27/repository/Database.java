package co.simplon.promo27.repository;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Classe permettant d'externaliser la connexion à la base de données pour pouvoir
 * la réutiliser dans les différents repositories
 */
public class Database {
    /**
     * Fait la connexion à la base de données en allant chercher la chaîne de caractère 
     * de connexion dans un fichier src/main/resources/application.properties
     * @return une instance de Connection à la base de données
     */
    public static Connection connect() throws SQLException {
        Properties props = new Properties();
        try {
            props.load(Database.class.getClassLoader().getResourceAsStream("application.properties"));

        } catch (IOException e) {
            System.out.println("Propertie file loading error");
            e.printStackTrace();
        }
        return DriverManager.getConnection(props.getProperty("database.url"));
    }
}
