# Java JDBC

Un projet Java Maven pour intéragir avec la base de données en utilisant JDBC

## How To Use 
1. Cloner le projet
2. Créer une base de données si pas déjà fait
3. Lancer le fichier [database](src/main/resources/database.sql) sur cette base de données


## Exercices
### Mise en place de la bdd et de l'entité
1. Via l'extension, ou une invite de commande ou autre, créer une base de données `p27_jdbc`
2. Dans le projet java, dans le fichier src/main/resources/database.sql faire le DROP et CREATE TABLE correspondant à la classe représentée au dessus (name et firstName not null, age nullable)
3. Dans le projet java, créer une classe co.simplon.promo27.entity.Person (donc une classe Person dans un nouveau package entity)
4. Faire que cette classe corresponde au diagramme de classe, avec donc 4 propriétés, toutes privées, getter/setter pour tout le monde et 3 constructeur : un vide, un plein, un sans id
5. On peut préparer le terrain en créant une nouvelle classe co.simplon.promo27.repository.PersonRepository (c'est là que nous mettrons les méthodes pour interagir avec la base de données)

### Le findById
1. Dans le PersonRepository créer une nouvelle méthode findById(int id)
2. En vous "inspirant" de la méthode findAll, faire cette fois ci un find by id, donc en modifiant la requête à exécuter (vous pouvez commencer par aller chercher l'id 1 par défaut directement dans la requête, sans utiliser l'argument id)
3. Modifier le while pour en faire plutôt un if et faire qu'on return la person ou null tout en bas. Pas besoin de List sur cette méthode
4. Une fois que ça marche pour la personne 1, on modifier la requête pour y mettre l'id

###  Externaliser la connexion et fichier de configuration
1. Créer dans le package repository une classe Database et dans cette classe créer une méthode  `public static Connection connect()` (en static pour qu'on puisse l'appeler sans avoir besoin d'instancier la classe Database)
2. Dans cette méthode connect, faire juste un return du DriverManager.get.... blablabla, il devrait vous souligner cette ligne en rouge, cliquer sur l'ampoule et choisir d'ajouter le throws
3. Dans le findAll et le findById, remplacer le DriverManager.get... par Database.connect()
4. Créer un fichier `application.properties` dans le dossier `src/main/resources` et dans ce fichier, mettre un `database.url=la chaine de caractère de connexion`
5. Dans la méthode connect() faire une instance de la classe Properties de java.util puis lancer sa méthode load() en lui donnant comme argument : `Database.class.getClassLoader().getResourceAsStream("application.properties")` et entourer le tout d'un try-catch avec l'ampoule
6. Dans le getConnection() au lieu de donner la String de connexion en dur, utiliser votre instance de properties.getProperty("database.url")

### Le delete d'une person
1. Dans PersonRepository, créer une nouvelle méthode delete(int id) qui renverra du booléen et qui fera une requête de suppression par id. Utiliser un placeholder et un setInt
2. À la place de faire un executeQuery() sur la requête, faire un executeUpdate(), cette méthode renvoie un int qui représente le nombre de ligne affectées
3. Faire en sorte de renvoyer true si il y a une ligne d'afféctée, sinon on renvoie false tout en bas de la méthode